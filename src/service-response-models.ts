export interface ServerError {
    message: string
    code?: string
    detail?: string
    errors?: ServerErrorItem[]
  }
  
  export interface ServerErrorItem {
    message: string
    propertyName?: string
  }
  
  export interface SuccessServiceResponse<T> {
    statusCode?: number
    response?: T
    headers?: any
  }
  
  export interface ErrorServiceResponse {
    serverError: ServerError
    statusCode: number
  }
  
  export type ServiceResponse<T> =
    | SuccessServiceResponse<T>
    | ErrorServiceResponse
  
  export function isErrorServiceResponse<T>(
    response: ServiceResponse<T>
  ): response is ErrorServiceResponse {
    return (response as ErrorServiceResponse).serverError !== undefined
  }
  
  export function notAuthorized(message?: string): ErrorServiceResponse {
    return {
      statusCode: 403,
      serverError: {
        message: message || 'Not Authorized',
      },
    }
  }
  