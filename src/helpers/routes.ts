import { Response } from 'express'
import {
  isErrorServiceResponse,
  ServiceResponse,
} from '../service-response-models'
import errorResponse from './error-response'

export function handleServiceResponse(
  res: Response,
  serviceResponse: ServiceResponse<{}>
) {
  if (isErrorServiceResponse(serviceResponse)) {
    errorResponse(
      res,
      serviceResponse.statusCode || 500,
      serviceResponse.serverError
    )
  } else {
    res.set('Cache-Control', 'no-cache, no-store, must-revalidate');
    if (serviceResponse.headers) {
      res.set(serviceResponse.headers)
    }
    res.status(serviceResponse.statusCode || 200)
    if (serviceResponse.response) {
      res.json(serviceResponse.response)
    } else {
      res.send()
    }
  }
}
