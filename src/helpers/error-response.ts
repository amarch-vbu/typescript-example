import { ServerError } from '../service-response-models'

export default function errorResponse(
  res: any,
  status: number,
  err: ServerError
) {
  res.status(status)
  res.json(err)
}
