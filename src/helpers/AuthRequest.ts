import {Request} from 'express';

export interface AuthRequest extends Request
{
    user: {
        sub: string,
        'https://claims.cimpress.io/canonical_id': string
    }
}