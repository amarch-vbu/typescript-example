export interface SchemaDto {
    schema: {
        name: string,
        schema: {}
    };
    name: string;
    id: string;
    lastModifiedTimestamp: string;
    owner: string;
    _links?: {
        self: {
          href: string
        }
    };
}