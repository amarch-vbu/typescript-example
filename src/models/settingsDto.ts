export interface SettingsDto {
    settings: any;
    name: string;
    id: string;
    resource: {
        type: string,
        id: string
    };
    _links?: {
        self: {
          href: string
        },
        schema?: {
            href: string
        }        
    };
}