import * as bodyParser from 'body-parser'
import * as cors from 'cors'
import * as express from 'express'
import * as auth0 from '@sapidus/auth0-middleware'
import * as middleware from './middleware'
import * as livecheck from './routes/livecheck'
import * as schemaController from './routes/v1/schemaController'
import * as settingController from './routes/v1/settingController'
import * as swagger from './routes/swagger';

export function createServer(): express.Express | null {

  const app = express();
  app.use('/livecheck', livecheck.createRouter());

  app.use(
    cors({
      origin: '*',
      exposedHeaders: ['Location', 'ETag'],
    })
  );
  
  // Support JSON encoded bodies.
  app.use(bodyParser.json());

  const schemaAuthOptions: auth0.Config = {
    unless : {
      method: ['GET', 'HEAD']
    }
  };

  app.use('/swagger', swagger.createRouter());

  app.use('/v1/schemas', auth0.getMiddleware(schemaAuthOptions));
  app.use('/v1/schemas', schemaController.createRouter());

  app.use('/v1/settings', auth0.getMiddleware());
  app.use('/v1/settings', settingController.createRouter());

  app.use(middleware.handleError);

  return app
}
