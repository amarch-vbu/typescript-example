require('dotenv').config();

import * as SumoLogger from 'sumo-logger';
import * as config from 'config'
import { SumoLoggerOptions } from 'sumo-logger';

export function createLogger(): SumoLogger {
  const options = config.get('sumoConfig');
  let endpoint = {};
  if(process.env.SUMO_URL){
    endpoint = {endpoint: process.env.SUMO_URL};
  }
  const opts = {...options, ...endpoint} as SumoLoggerOptions;
  return new SumoLogger(opts);
}