import * as express from 'express'
import { createLogger } from './logger';

const logger = createLogger();

export function handleError(
  error: any,
  req: express.Request,
  res: express.Response,
  _next: express.NextFunction
) {
  const message = {
    err: error.message,
    innerErr: error ? error.innerError : undefined,
  }

  if (error.name === 'UnauthorizedError') {
    res.status(401)
    res.setHeader('WWW-Authenticate', 'Bearer realm="https://api.cimpress.io/", authorization_uri="https://cimpress.auth0.com/oauth/token"')
    res.json({
      errors: [
        {
          message: 'Not Authenticated',
        },
      ],
    })

    return
  }

  logger.log({request: req.originalUrl, body: req.body, error: message, statusCode: 500});

  res.status(500)
  const response = {
    errors: [
      {
        message: 'Internal Server Error',
      },
    ],
  }
  res.json(response)
}
