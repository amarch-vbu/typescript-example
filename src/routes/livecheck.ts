import { Router } from 'express'

export function createRouter(): Router {
  const router = Router()

  router.route('/').get((_req, res) => res.send('Running!'))

  return router
}
