import { Router } from 'express'
import { readFileSync } from 'fs'
import { wrap } from 'async-middleware'

const schemaYaml = readFileSync('./resources/swagger.yaml')

export function createRouter(): Router {
  const router = Router()

  router.get(
    '/',
    wrap(async (_req, res, _next) => {
      res.set('content-type', 'text/yaml').send(schemaYaml);
    })
  );

  return router
}