import { Router } from 'express'
import { wrap } from 'async-middleware'
import { getSchemaById, putSchema, postSchema, getSchemaByName } from '../../AwsUtil/SchemaClient';
import { SuccessServiceResponse, ServiceResponse, notAuthorized, ErrorServiceResponse } from '../../service-response-models';
import { handleServiceResponse } from '../../helpers/routes';
import { canCreateSchema } from '../../coamUtil';
import { AuthRequest } from '../../helpers/AuthRequest';
import * as Ajv from 'ajv';
import { SchemaDto } from '../../models/schemaDto';
import * as config from 'config';

async function validateSchemaWrite(req: AuthRequest): Promise<ErrorServiceResponse | undefined> {

  // Check if user has permissons to write
  const canWrite = await canCreateSchema(req.user.sub, req.headers.authorization as string);
  if (!canWrite) {
    return notAuthorized("User requires settings:* manage:schemas permissons to write schemas");
  }

  const schemaDto = req.body as SchemaDto;
  if (!schemaDto.schema) {
    return  {
      statusCode: 400,
      serverError: {
          message: `Invalid schema request. Field schema is missing`
      }
    };
  }

  if (!schemaDto.name) {
    return  {
      statusCode: 400,
      serverError: {
          message: `Invalid schema request. Field name is missing`
      }
    };
  }

  if (schemaDto.name.includes(':')) {
    return  {
      statusCode: 400,
      serverError: {
          message: `Invalid schema request. Name field can not contain character ':'`
      }
    };
  }

  const ajv = new Ajv();
  const isValid = ajv.validateSchema(schemaDto.schema);
  if(!isValid) {
    return  {
      statusCode: 400,
      serverError: {
          message: `Invalid schema. Please verify the schema meets JSON Schema draft-07 specification`
      }
    };
  }

  return undefined;
}

export function createSchemaLink(schemaId: string): string {
  return `${config.get('host')}/v1/schemas/${schemaId}`
}

interface schemaLinks {
  self: {
    href: string
  }
}

export function createRouter(): Router {
  const router = Router()

  router.get('/:schemaId',
    wrap(async (req, res, _next) => {
      const schema = await getSchemaById(req.params.schemaId);

      let result: ServiceResponse<{}>;

      if(schema) {
        let links: schemaLinks = {
          self: {
            href: createSchemaLink(req.params.schemaId)
          }
        };
        schema._links = links;

        result = {
          statusCode: 200,
          response: schema as {}
        };
      }
      else {
        result = {
          statusCode: 404,
          serverError: {
            message: `No schema with Id ${req.params.schemaId} found`
          }
        }
      }

      handleServiceResponse(res, result);
    })
  );

  router.get('/',
    wrap(async (req, res, _next) => {

      let schemas = await getSchemaByName(req.query.name);

      schemas.forEach(function(_, index, __) {
        schemas[index]._links = {
          self: {
            href: createSchemaLink(schemas[index].id)
          }
        };
      });

      const hal_resp = {
        _embedded: {
          item: schemas
        }
      };
      const result: SuccessServiceResponse<{}> = {
        statusCode: 200,
        response: hal_resp
      };
      handleServiceResponse(res, result);
    })
  );

  router.post('/',
    wrap(async (req, res, _next) => {
      const reqAuth = req as AuthRequest;
      let result: ServiceResponse<{}>;

      const error = await validateSchemaWrite(reqAuth);

      if(error === undefined) {
        const schemaId = await postSchema(req.body.name, req.body, reqAuth.user["https://claims.cimpress.io/canonical_id"]);      

        if(schemaId) {
          result= {
            statusCode: 201,
            headers: {Location: `${config.get('host')}/v1/schemas/${schemaId}`}
          };
        }
        else {
          result= {
            statusCode: 400,
            serverError: {
              message: `Schema with name ${req.body.name} already exists`
            }
          };
        }
      }
      else {
        result = error!;
      }

      handleServiceResponse(res, result);
    })
  );

  router.put('/:schemaId',
    wrap(async (req, res, _next) => {
      const reqAuth = req as AuthRequest;
      let result: ServiceResponse<{}>;

      let error = await validateSchemaWrite(reqAuth);

      // Need to get schema to make sure the name doesn't change
      const schema = await getSchemaById(req.params.schemaId);
      if(schema && schema.name !== req.body.name) {
        error = {
          statusCode: 400,
          serverError: {
              message: `The name of the schema cannot be change from ${schema.name}.`
          }
        };
      }

      if (error === undefined) {

          const success = await putSchema(req.params.schemaId, req.body, reqAuth.user["https://claims.cimpress.io/canonical_id"]);
          if(success) {
            result = {
              statusCode: 200,
              headers: {Location: `${config.get('host')}/v1/schemas/${req.params.schemaId}`}
            };
          }
          else {
            result = {
              statusCode: 404,
              serverError: {
                  message: `Unable to find schema with id ${req.params.schemaId}`
              }
            };
          }
      }
      else {
        result = error!;
      }

      handleServiceResponse(res, result);
    })
  );

  return router
}
