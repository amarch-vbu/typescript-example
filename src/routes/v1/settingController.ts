import { Router } from 'express'
import { wrap } from 'async-middleware'
import { getSettingById, getSettingByName, putSetting, postSetting } from '../../AwsUtil/SettingClient';
import { getSchemaByName } from '../../AwsUtil/SchemaClient';
import { ServiceResponse, notAuthorized, ErrorServiceResponse } from '../../service-response-models';
import { handleServiceResponse } from '../../helpers/routes';
import { canCreateSettings, canReadSettings } from '../../coamUtil';
import { AuthRequest } from '../../helpers/AuthRequest';
import * as Ajv from 'ajv';
import { SettingsDto } from '../../models/settingsDto';
import * as config from 'config';
import { SchemaDto } from '../../models/schemaDto';
import { createSchemaLink } from './schemaController';

function createSettingId(resourceType: string, resourceId: string, settingId: string): string {
    return `${resourceType}:${resourceId}:${settingId}`;
}

function parseSettingName(settingName: string) {
  const splits = settingName.split(':');
  return {
    resouceType : splits[0],
    resourceId: splits[1],
    settingName: splits[2]
  };
}

async function validateSettingsWrite(req: AuthRequest, tenantInfo: { resourceId: string; resourceType: string; settingName: string; }, settings: SettingsDto): Promise<ErrorServiceResponse | undefined> {
  if(! settings.settings ) {
    return {
      statusCode: 400,
      serverError: {
        message: `Missing required field settings`
      }
    };
  }

  if(! settings.name ) {
    return {
      statusCode: 400,
      serverError: {
        message: `Missing required field name`
      }
    };
  }

  if(! settings.resource ) {
    return {
      statusCode: 400,
      serverError: {
        message: `Missing required field resource`
      }
    };
  }
  
  // Check if user has permissons
  const auth = await canCreateSettings(tenantInfo.resourceId, tenantInfo.resourceType, req.user.sub, req.headers.authorization as string, req.user["https://claims.cimpress.io/canonical_id"], tenantInfo.settingName);
  if(!auth) {
    return notAuthorized();
  }

  // Look for schema and validate against it
  // Do we want to require a schema?
  const schemaDto = await getSchemaQueryName(tenantInfo.settingName);
  if (schemaDto !== undefined) {
    const ajv = new Ajv();
    const valid = ajv.validate(schemaDto!.schema.schema, settings.settings);
    if (!valid) {
      return {
        statusCode: 400,
        serverError: {
          message: `Settings to don't follow schema. Please validate against schema name ${tenantInfo.settingName}`
        }
      };
    }
  }

  return undefined;
}

async function getSchemaQueryName(name: string): Promise<undefined | SchemaDto> {
  const schemaDto = await getSchemaByName(name);
  if (schemaDto !== undefined && schemaDto.length === 1) {
    return schemaDto[0];
  }
  return undefined;
}

function createSettingLink(settingId: string): string {
  return `${config.get('host')}/v1/settings/${settingId}`
}

export function createRouter(): Router {
  const router = Router()

  router.get('/:settingId',
    wrap(async (req, res, _next) => {
      const reqAuth = req as AuthRequest;
      const canonicalId = reqAuth.user["https://claims.cimpress.io/canonical_id"];
      let result: ServiceResponse<{}>;

      let settings = await getSettingById(req.params.settingId);

      if (settings) {
        const settingInfo = parseSettingName(settings!.name);

        const canRead = await canReadSettings(settingInfo.resourceId, settingInfo.resouceType, reqAuth.user.sub, req.headers.authorization as string, canonicalId, settingInfo.settingName);
      
        if(!canRead) {
          result = notAuthorized();
        }
        else {

          const schema = await getSchemaQueryName(settings.settings.name);
          let schemaLinks = undefined;
          if(schema) {
            schemaLinks = {
              href: createSchemaLink(schema.id)
            }
          }

          settings._links = {
            self: {
              href: createSettingLink(req.params.settingId)
            },
            schema: schemaLinks
          };

          result = {
            statusCode: 200,
            response: settings
          };
       }
      }
      else {
        result = {
          statusCode: 404,
          serverError: {
            message: `No setting with Id: ${req.params.settingId}`
          }
        };
      }
      handleServiceResponse(res, result);
    })
  );

  router.get('/',
    wrap(async (req, res, _next) => {
      const reqAuth = req as AuthRequest;
      const canonicalId = reqAuth.user["https://claims.cimpress.io/canonical_id"];

      const canRead = await canReadSettings(req.query.resourceId, req.query.resourceType, reqAuth.user.sub, req.headers.authorization as string, canonicalId, req.query.settingName);
      let result: ServiceResponse<{}>;
      if(!canRead) {
        result = notAuthorized();
      }
      else {
        const settingId = createSettingId(req.query.resourceType, req.query.resourceId, req.query.settingName);
        let settingsCollection = await getSettingByName(settingId);

        if(settingsCollection) {
          let settingsCollectionVerified = settingsCollection as SettingsDto[];
          settingsCollectionVerified.forEach(function(_, index, __) {
            settingsCollectionVerified[index]._links = {
              self: {
                href: createSchemaLink(settingsCollectionVerified[index].id)
              }
            };
          });
        }


        const hal_Resp = {
          _embedded : {
            item: settingsCollection
          }
        };
        result = {
          statusCode: 200,
          response: hal_Resp
        };
     }
      handleServiceResponse(res, result);
    })
  );

  router.post('/',
    wrap(async (req, res, _next) => {
      const reqAuth = req as AuthRequest;
      let result: ServiceResponse<string>;

      const settingsDto = req.body as SettingsDto;
      const reqInfo = {
        resourceId: settingsDto.resource.id,
        resourceType: settingsDto.resource.type,
        settingName: settingsDto.name
      };
      
      const auth = await validateSettingsWrite(reqAuth, reqInfo, settingsDto);


      if(auth === undefined) {
        const settingId = createSettingId(req.body.resource.type, req.body.resource.id, req.body.name);
        const dbId = await postSetting(settingId, settingsDto, reqAuth.user["https://claims.cimpress.io/canonical_id"]);
        if(dbId) {
          result = {
            statusCode: 201,
            headers: {Location: `${config.get('host')}/v1/settings/${dbId}`}
          };
        }
        else {
          result= {
            statusCode: 400,
            serverError: {
              message: `Schema with name ${req.body.name} already exists`
            }
          };
        }
        
      }
      else {
        result = auth!;
      }

      handleServiceResponse(res, result);
    })
  );

  router.put('/:settingId',
    wrap(async (req, res, _next) => {
      const reqAuth = req as AuthRequest;
      let result: ServiceResponse<string>;

      const settings = await getSettingById(req.params.settingId);

      const newSettings = req.body as SettingsDto;

      if(settings && settings.settings.name !== newSettings.name) {
        result = {
          statusCode: 400,
          serverError: {
            message: `settings name cannot be changed from ${settings.settings.name}`
          }
        };
      }
      else if(settings) {
        const reqInfo = {
          resourceId: settings.settings.resource.id,
          resourceType: settings.settings.resource.type,
          settingName: settings.settings.name
        };
        const auth = await validateSettingsWrite(reqAuth, reqInfo, newSettings);
        if(auth === undefined) {
          await putSetting(req.params.settingId, newSettings, reqAuth.user["https://claims.cimpress.io/canonical_id"]);
          result = {
            statusCode: 200,
            headers: {Location: `${config.get('host')}/v1/settings/${req.params.settingId}`}
          };
        }
        else {
          result = auth!;
        }
      }
      else {
        result = {
          statusCode: 404,
          serverError: {
            message: `settings with Id ${req.params.settingId} does not exist`
          }
        };
      }
      handleServiceResponse(res, result);
    })
  );

  return router
}
