import 'newrelic'

import * as aws from 'aws-sdk';
import * as sourceMapSupport from 'source-map-support'
// starting sourceMap support as soon as possible to have stacktrace in TypeScript
sourceMapSupport.install()

import * as config from 'config'
import { createServer } from './server'
import { createLogger } from './logger';
const logger = createLogger();

const server = createServer();
const PORT = config.get('port');

if (server) {
  if(process.env.NODE_ENV === 'test') {
    const credentials = new aws.SharedIniFileCredentials({profile: 'documents'});
    aws.config.credentials = credentials;
  }
  aws.config.update({region: config.get('region')});
  server.listen(PORT);
  logger.log(
    `Started listening on port ${PORT}. Environment: ${process.env.NODE_ENV ||
      'development'}`
  );
}
