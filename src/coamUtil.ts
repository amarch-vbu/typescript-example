import * as coam from '@sapidus/coam-sapidus';

export async function canCreateSchema(
    princial: string,
    authorization: string
    ): Promise<boolean> {
        // user needs to be user user
        return coam.auth.isAllowed('*', 'settings', princial, "manage:schemas", authorization, "0" );
  }

  export async function canReadSettings (
    resourceId: string,
    resourceType: string,
    princial: string,
    authorization: string,
    canonicalId: string,
    settingName: string
    ): Promise<boolean> {
        if(resourceType == 'users') {
          // if canonical id is the same as the route return true else check if user is super user
          return canonicalId === resourceId ? Promise.resolve(true) : coam.auth.isAllowed('*', 'settings', princial, "read:setting", authorization, "0" );
        }
        const permissions = await Promise.all([idOrWildCard(resourceId, resourceType, princial, authorization, 'read:setting'), idOrWildCard(settingName, 'settings', princial, authorization, 'read:setting')]);
        return permissions[0] && permissions[1];
  }

  export async function canCreateSettings (
    resourceId: string,
    resourceType: string,
    principal: string,
    authorization: string,
    canonicalId: string,
    settingId: string
    ): Promise<boolean> {
        if(resourceType == 'users') {
          // if canonical id is the same as the route return true else check if user is super user
          return canonicalId === resourceId ? Promise.resolve(true) : coam.auth.isAllowed('*', 'settings', principal, "manage:settings", authorization, "0" );
        }
        return Promise.all([idOrWildCard(resourceId, resourceType, principal, authorization, 'manage:setting'), idOrWildCard(settingId, 'settings', principal, authorization, 'manage:setting')])
          .then(([tenentAuth, settingsAuth]) => {
            return tenentAuth && settingsAuth;
          });
  }

  async function idOrWildCard(
    resourceId: string,
    resourceType: string,
    principal: string,
    authorization: string,
    permission: string
  ): Promise<boolean> {
    const res = await Promise.all([coam.auth.isAllowed(resourceId, resourceType, principal, permission, authorization, "0" ), coam.auth.isAllowed('*', resourceType, principal, permission, authorization, "0" )])
    return res[0] || res[1];
  }
  