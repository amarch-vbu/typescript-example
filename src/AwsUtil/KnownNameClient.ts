import * as config from 'config';
import { DynamoDB } from 'aws-sdk';

export async function reserveName(id: string, owner: string): Promise<boolean> {
    const dbClient = new DynamoDB.DocumentClient();
    const lower_id = id.toLowerCase();
    let params = {
        TableName: config.get("NamesDb") as string,
        Item: {
            id: lower_id,
            owner
        },
        ExpressionAttributeNames: {
            '#id': 'id'
        },
        ConditionExpression: 'attribute_not_exists(#id)'
    };

    return await dbClient.put(params).promise().then(
        () => true
    ).catch(() => false);
};