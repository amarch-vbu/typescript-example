import * as config from 'config';
import { DynamoDB, AWSError } from 'aws-sdk';
import * as uuid from 'uuid';
import * as moment from 'moment';
import { reserveName } from './KnownNameClient';
import { QueryOutput } from 'aws-sdk/clients/dynamodb';
import { SchemaDto } from '../models/schemaDto';

const dbClientConfig: DynamoDB.DocumentClient.DocumentClientOptions = {
    convertEmptyValues: true
};

export async function getSchemaById(id: string): Promise<SchemaDto | null> {
    const dbClient = new DynamoDB.DocumentClient(dbClientConfig);
    const params: DynamoDB.DocumentClient.GetItemInput = {
        TableName: config.get("SchemaDb"),
        Key: {
            id
        }
    };

    return await dbClient.get(params).promise().then(
        (data) => data.Item as any
    );      
}

export async function getSchemaByName(settingName: string): Promise<SchemaDto[]> {
    const dbClient = new DynamoDB.DocumentClient(dbClientConfig);
    const params: DynamoDB.DocumentClient.QueryInput = {
        TableName: config.get("SchemaDb"),
        IndexName: 'name-index',
        KeyConditionExpression: '#name = :name',
        ExpressionAttributeNames:{
            '#name': 'name'
        },
        ExpressionAttributeValues: {
            ":name": settingName
        }
    };

    return await dbClient.query(params).promise().then(
        (data: QueryOutput) => data.Items as any
    );
}

export async function putSchema(id: string, schema: {}, owner: string): Promise<boolean> {
    const dbClient = new DynamoDB.DocumentClient(dbClientConfig);
    const modifiedAt = moment().utc().toISOString();

    let params = {
        Key: { // The primary key of the item (a map of attribute name to AttributeValue)
            id
        },
        TableName: config.get("SchemaDb") as string,
        ExpressionAttributeNames: {
            '#id': 'id',
            '#schema': 'schema',
            '#modifiedAt': 'modifiedAt',
            '#modifiedBy': 'modifiedBy'
        },
        ExpressionAttributeValues: {
            ':schema': schema,
            ':modifiedAt': modifiedAt,
            ':modifiedBy': owner
        },
        ConditionExpression: 'attribute_exists(#id)',
        UpdateExpression: "set #schema = :schema, #modifiedAt = :modifiedAt, #modifiedBy = :modifiedBy",
    };

    return await dbClient.update(params).promise().then(
        () => true
    ).catch((error: AWSError) => {
        // TODO logging
        return false;
    });
}

export async function postSchema(settingName: string, schema: {}, createdBy: string): Promise<string | undefined> {

    const reserved = await reserveName(settingName, createdBy);

    if (!reserved) {
        return undefined;
    }

    const dbClient = new DynamoDB.DocumentClient(dbClientConfig);
    const timeStamp = moment().utc().toISOString();
    const id = uuid();
    let params = {
        TableName: config.get("SchemaDb") as string,
        Item: {
            schema,
            name: settingName,
            id,
            modifiedAt: timeStamp,
            createdBy,
            createdAt: timeStamp,
            modifiedBy: createdBy
        }
    };

    return await dbClient.put(params).promise().then(
        () => id
    )
}