import * as config from 'config';
import { DynamoDB } from 'aws-sdk';
import * as uuid from 'uuid';
import * as moment from 'moment';
import { reserveName } from './KnownNameClient';
import { SettingsDto } from '../models/settingsDto';

const dbClientConfig: DynamoDB.DocumentClient.DocumentClientOptions = {
    convertEmptyValues: true
};

export async function getSettingById(id: string): Promise<SettingsDto | null> {
    const dbClient = new DynamoDB.DocumentClient(dbClientConfig);
    const params: DynamoDB.DocumentClient.GetItemInput = {
        TableName: config.get("SettingDb"),
        Key: {
            id
        }
    };

    return dbClient.get(params).promise().then(
        (data) => data.Item as any
    );
};

export async function getSettingByName(settingName: string): Promise<SettingsDto[] | null> {
    const dbClient = new DynamoDB.DocumentClient(dbClientConfig);
    const params: DynamoDB.DocumentClient.QueryInput = {
        TableName: config.get("SettingDb"),
        IndexName: 'name-index',
        KeyConditionExpression: '#name = :name',
        ExpressionAttributeNames: {
            '#name': 'name'
        },
        ExpressionAttributeValues: {
            ":name": settingName
        }
    };

    return await dbClient.query(params).promise().then(
        (data) => data.Items as any
    );
};

export async function putSetting(id: string, settings: {}, owner: string): Promise<string> {
    const dbClient = new DynamoDB.DocumentClient(dbClientConfig);
    const modifiedAt = moment().utc().toISOString();

    let params = {
        Key: { // The primary key of the item (a map of attribute name to AttributeValue)
            id
        },
        TableName: config.get("SettingDb") as string,
        ExpressionAttributeNames: {
            '#id': 'id',
            '#settings': 'settings',
            '#modifiedAt': 'modifiedAt',
            '#modifiedBy': 'modifiedBy'
        },
        ExpressionAttributeValues: {
            ':settings': settings,
            ':modifiedAt': modifiedAt,
            ':modifiedBy': owner
        },
        ConditionExpression: 'attribute_exists(#id)',
        UpdateExpression: 'set #settings = :settings, #modifiedAt = :modifiedAt, #modifiedBy = :modifiedBy'
    };

    await dbClient.update(params).promise();
    return id;
};

export async function postSetting(settingName: string, settings: {}, createdBy: string): Promise<string | undefined> {

    const reserved = await reserveName(settingName, createdBy);

    if (!reserved) {
        return undefined;
    }

    const dbClient = new DynamoDB.DocumentClient(dbClientConfig);
    const timeStamp = moment().utc().toISOString();
    const id = uuid();
    let params = {
        TableName: config.get("SettingDb") as string,
        Item: {
            settings,
            name: settingName,
            id,
            modifiedAt: timeStamp,
            createdBy,
            createdAt: timeStamp,
            modifiedBy: createdBy
        }
    };

    await dbClient.put(params).promise();
    return id;
}