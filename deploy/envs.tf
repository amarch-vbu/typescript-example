locals {
    env = {
        defaults = {
        name         = "${terraform.workspace}"
        environment         = "${terraform.workspace}"
        region              = "eu-west-1"
        cluster_name        = "${terraform.workspace}-doc-apps"
        app_port            = 3000
        app_count           = 1
        app_image           = "918895503574.dkr.ecr.eu-west-1.amazonaws.com/settingservice:0.9.261541"
        ecs_autoscale_role  =  "arn:aws:iam::918895503574:role/AWSApplicationAutoscalingECSServicePolicy"
        ecs_task_execution_role = "arn:aws:iam::918895503574:role/ecsTaskExecutionRole"
        ecs_task_role       = "arn:aws:iam::918895503574:role/setting-service-task-role"
        fargate_cpu         = 1024
        fargate_memory      = 2048
        host_url            = "dev.settings.cimpress.io"
        task_name           = "setting-service-${terraform.workspace}"
      }
  
      dev = {
        environment         = "dev",
        nodeenv             = "dev"
      }
  
      prod = {
        fargate_cpu         = 2048
        fargate_memory      = 4096
        host_url            = "settings.cimpress.io"
        app_count           = 2
        nodeenv             = "prod"
        environment         = "prod"
      }
    }
  
    workspace = "${merge(local.env["defaults"], local.env[terraform.workspace])}"
    app_image           = "${local.workspace["app_image"]}"
    environment         = "${local.workspace["environment"]}"
    region              = "${local.workspace["region"]}"
    cluster_name        = "${local.workspace["cluster_name"]}"
    app_port            = "${local.workspace["app_port"]}"
    app_count           = "${local.workspace["app_count"]}"
    ecs_autoscale_role  = "${local.workspace["ecs_autoscale_role"]}"
    ecs_task_role       = "${local.workspace["ecs_task_role"]}"
    ecs_task_execution_role  = "${local.workspace["ecs_task_execution_role"]}"
    fargate_cpu         = "${local.workspace["fargate_cpu"]}"
    fargate_memory      = "${local.workspace["fargate_memory"]}"

    nodeenv             = "${local.workspace["nodeenv"]}"
    host_url            = "${local.workspace["host_url"]}"
    task_name           = "${local.workspace["task_name"]}"
    
  }
  