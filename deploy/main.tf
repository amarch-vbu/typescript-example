provider "aws" {
  region = "${local.region}" # The aws region
}
# Fetch AZs in the current region
data "aws_availability_zones" "available" {}

data "template_file" "app_task_file" {
  template = "${file("task.json.tpl")}"

  vars {
    app_image      = "${local.app_image}"
    fargate_cpu    = "${local.fargate_cpu}"
    fargate_memory = "${local.fargate_memory}"
    aws_region     = "${local.region}"
    app_port       = "${local.app_port}"
    environment    = "${local.environment}"
    nodeenv        = "${local.nodeenv}"
    cluster_name   = "${local.cluster_name}"
    host_url       = "${local.host_url}"
    task_name      = "${local.task_name}"
  }
}

resource "aws_ecs_task_definition" "app" {
  family                   = "${local.task_name}"
  task_role_arn            = "${local.ecs_task_role}"
  execution_role_arn       = "${local.ecs_task_execution_role}"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = "${local.fargate_cpu}"
  memory                   = "${local.fargate_memory}"
  container_definitions    = "${data.template_file.app_task_file.rendered}"
  tags                     = {
    region          = "${local.region}"
    environment     = "${local.environment}"
    workspace       = "${terraform.workspace}"
    app             = "setting-service"
  }
}

data "aws_ecs_task_definition" "app" {
  task_definition = "${aws_ecs_task_definition.app.family}"
}

data "aws_lb_listener" "front_end" {
  load_balancer_arn = "${data.aws_lb.main.arn}"
  port              = "443"
}

resource "aws_ecs_service" "main" {
  name            = "${local.cluster_name}-setting-service"
  cluster         = "arn:aws:ecs:eu-west-1:918895503574:cluster/${local.cluster_name}"
  task_definition = "${aws_ecs_task_definition.app.family}:${max("${aws_ecs_task_definition.app.revision}", "${data.aws_ecs_task_definition.app.revision}")}"
  desired_count   = "${local.app_count}"
  launch_type     = "FARGATE"
  health_check_grace_period_seconds = 5

  tags                     = {
    region          = "${local.region}"
    environment     = "${local.environment}"
    workspace       = "${terraform.workspace}"
    app             = "setting-service"
  }

  network_configuration {
    security_groups  = ["${aws_security_group.ecs_tasks.id}"]
    subnets          = ["${data.aws_subnet_ids.private.ids}"]
    assign_public_ip = false
  }

  load_balancer {
    target_group_arn = "${aws_alb_target_group.app.id}"
    container_name   = "setting-service"
    container_port   = "${local.app_port}"
  }

  depends_on = [
    "aws_lb_listener_rule.host_based_routing",
  ]
  
  lifecycle {
    ignore_changes = ["desired_count"]
  }
}

data "aws_vpc" "main" {
    tags {
        VPC = "Default"
    }
}

data "aws_subnet_ids" "public" {
    vpc_id = "${data.aws_vpc.main.id}"
    tags {
        Public = ""
    }
}

data "aws_subnet_ids" "private" {
    vpc_id = "${data.aws_vpc.main.id}"
    tags {
        Private = ""
    }
}

resource "aws_security_group" "ecs_tasks" {
  name        = "${local.cluster_name}-setting-service-ecs-sg"
  description = "allow inbound access from the ALB only"
  vpc_id      = "${data.aws_vpc.main.id}"

  ingress {
    protocol        = "tcp"
    from_port       = "${local.app_port}"
    to_port         = "${local.app_port}"
    security_groups = ["${data.aws_security_group.lb.id}"]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

data "aws_security_group" "lb" {
  name        = "${local.cluster_name}-alb-sg"
}

data "aws_lb" "main" {
  name        = "${local.cluster_name}-alb"
}

resource "aws_alb_target_group" "app" {
  name        = "${local.cluster_name}-setting-service-tg"
  port        = "${local.app_port}"
  protocol    = "HTTP"
  vpc_id      = "${data.aws_vpc.main.id}"
  target_type = "ip"
  deregistration_delay = 5

  health_check {
    healthy_threshold   = "3"
    interval            = "30"
    protocol            = "HTTP"
    matcher             = "200"
    timeout             = "5"
    path                = "/livecheck"
    unhealthy_threshold = "2"
  }

  
  tags                     = {
    region          = "${local.region}"
    environment     = "${local.environment}"
    workspace       = "${terraform.workspace}"
    app             = "settingService"
  }
}

data "aws_acm_certificate" "documents" {
  domain   = "settings.cimpress.io"
  statuses = ["ISSUED"]
  most_recent = true
}

resource "aws_appautoscaling_target" "ecs_target" {
  service_namespace  = "ecs"
  resource_id        = "service/${local.cluster_name}/${aws_ecs_service.main.name}"
  scalable_dimension = "ecs:service:DesiredCount"
  role_arn           = "${local.ecs_autoscale_role}"
  min_capacity       = 1
  max_capacity       = 3
}

resource "aws_appautoscaling_policy" "ecs_policy" {
  depends_on    = ["aws_appautoscaling_target.ecs_target"]
  name        = "CPU_Scaling"
  policy_type = "TargetTrackingScaling"
  resource_id        = "service/${local.cluster_name}/${aws_ecs_service.main.name}"

  scalable_dimension = "${aws_appautoscaling_target.ecs_target.scalable_dimension}"

  service_namespace = "ecs"
 
  target_tracking_scaling_policy_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageCPUUtilization"
    }

    scale_out_cooldown = 30
    scale_in_cooldown  = 120
    target_value       = 50.0
  }

  depends_on = ["aws_appautoscaling_target.ecs_target"]
}

resource "aws_lb_listener_certificate" "documents" {
  listener_arn    = "${data.aws_lb_listener.front_end.arn}"
  certificate_arn = "${data.aws_acm_certificate.documents.arn}"
}

resource "aws_lb_listener_rule" "host_based_routing" {
  listener_arn = "${data.aws_lb_listener.front_end.arn}"
  priority     = 99

  action {
    type             = "forward"
    target_group_arn = "${aws_alb_target_group.app.arn}"
  }

  condition {
    field  = "host-header"
    values = ["${local.host_url}"]
  }
}

/*
# Set up cloudwatch group and log stream and retain logs for 14 days
resource "aws_cloudwatch_log_group" "log_group" {
  name              = "/ecs/${local.cluster_name}-setting-service-fargate"
  retention_in_days = 14

   tags                     = {
    region          = "${local.region}"
    environment     = "${local.environment}"
    workspace       = "${terraform.workspace}"
    app             = "setting-service"
  }
}

resource "aws_cloudwatch_log_stream" "log_stream" {
  name           = "${local.cluster_name}-log-stream"
  log_group_name = "${aws_cloudwatch_log_group.log_group.name}"
}
*/