[
  {
    "name": "setting-service",
    "image": "${app_image}",
    "cpu": ${fargate_cpu},
    "memory": ${fargate_memory},
    "networkMode": "awsvpc",
    "environment": [
      {
        "name": "NODE_ENV",
        "value": "${nodeenv}"
      },
      {
        "name": "NEW_RELIC_APP_NAME",
        "value": "Doc.Setting.Service (${environment})"
      },
      {
        "name": "NEW_RELIC_LICENSE_KEY",
        "value": "46dd531826787e94997364859699805050bb702d"
      },
      {
        "name": "NEW_RELIC_NO_CONFIG_FILE",
        "value": "true"
      }
    ],
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-group": "/ecs/${cluster_name}-setting-service-fargate",
        "awslogs-region": "${aws_region}",
        "awslogs-stream-prefix": "ecs"
      }
    },
    "portMappings": [
      {
        "containerPort": ${app_port},
        "hostPort": ${app_port}
      }
    ]
  }
]
