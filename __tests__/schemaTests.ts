import { createServer } from "../src/server";
import * as http from "http";
import * as express from 'express';
import axios, { AxiosRequestConfig } from 'axios';
import { schemaProvided, idFromUrl, cleanSchema } from "./resources";
import * as uuid from 'uuid';
import * as aws from 'aws-sdk';
import * as config from 'config'
import { SchemaDto } from "../src/models/schemaDto";
require('dotenv').config();

describe('testing schema endpoints, coam disabled', () => {
    let service: http.Server;
    let server: express.Express;
    const testPort = 3000;
    beforeAll(() => {
        if(process.env.LOCAL) {
            const credentials = new aws.SharedIniFileCredentials({profile: 'documents'});
            aws.config.credentials = credentials;
        }
        aws.config.update({region: config.get('region')});
        server = createServer() as express.Express;
        if (server) {
            service = server.listen(testPort);
        }
    });

    afterAll( () => {
        service.close();
    });

    const noAuthOptions: AxiosRequestConfig = {
        headers: {
            authorization : "no"
         },
         validateStatus: () => true // As long as the request returns, no error will be thrown
    };

    const authOptions: AxiosRequestConfig = {
        headers: {
            authorization : "yes"
         },
         validateStatus: () => true
    };

    it('should not allow post schema unauthorized', async () => {
        const res = await axios.post(`http://localhost:${testPort}/v1/schemas/`, schemaProvided, noAuthOptions);
        // Force to fail if catch wasn't hit.
        expect(res.status).toEqual(403);
    });

    it('find schema by id', async () => {
        let newSchema = schemaProvided
        newSchema.name = uuid().toLowerCase();
        const location = await axios.post(`http://localhost:${testPort}/v1/schemas/`, newSchema, authOptions)
            .then(response => {
                expect(response.status).toEqual(201);
                expect(response.headers.location).toBeDefined();
                return response.headers.location;
            });
        const schemaId = idFromUrl(location);
        const getResponse = await axios.get(`http://localhost:${testPort}/v1/schemas/${schemaId}`);

        expect(getResponse.status).toEqual(200);
        expect(getResponse.data.id).toEqual(schemaId);
        expect(getResponse.data.name).toEqual(newSchema.name);
        expect(getResponse.data.schema).toBeDefined();

        await cleanSchema(newSchema.name, schemaId);
    });

    it('find schema by id 404', async () => {
        const schemaId = uuid().toLowerCase();
        const getResponse = await axios.get(`http://localhost:${testPort}/v1/schemas/${schemaId}`, noAuthOptions);

        expect(getResponse.status).toEqual(404);
    });

    it('find schema by name', async () => {
        let newSchema = schemaProvided
        newSchema.name = uuid().toLowerCase();
        await axios.post(`http://localhost:${testPort}/v1/schemas/`, newSchema, authOptions)
            .then(response => {
                expect(response.status).toEqual(201);
                expect(response.headers.location).toBeDefined();
            });

        const schemaRes = await axios.get(`http://localhost:${testPort}/v1/schemas?name=${newSchema.name}`);
        expect(schemaRes.status).toEqual(200);

        const data = schemaRes.data;
        expect(data).toBeDefined();
        expect(data._embedded).toBeDefined();
        expect(data._embedded.item).toBeDefined();

        const items = data._embedded.item as SchemaDto[];
        expect(items.length).toEqual(1);

        const schema = items[0];
        expect(schema.name).toEqual(newSchema.name);
        expect(schema.schema).toBeDefined();
        
        await cleanSchema(newSchema.name, schema.id);
    });

    it('should allow PUT to existing schema', async () => {
        let newSchema = schemaProvided
        newSchema.name = uuid().toLowerCase();
        const schemaId = await axios.post(`http://localhost:${testPort}/v1/schemas/`, schemaProvided, authOptions)
            .then( async (response) => {
                expect(response.status).toEqual(201);
                return idFromUrl(response.headers.location);
        });

        await axios.put(`http://localhost:${testPort}/v1/schemas/${schemaId}`, schemaProvided, authOptions)
        .then( async (response) => {
            expect(response.status).toEqual(200);
        });

        await cleanSchema(newSchema.name, schemaId);
    });

    it('should not allow PUT to change schema name', async () => {
        let newSchema = schemaProvided
        newSchema.name = uuid().toLowerCase();
        const schemaId = await axios.post(`http://localhost:${testPort}/v1/schemas/`, newSchema, authOptions)
            .then( async (response) => {
                expect(response.status).toEqual(201);
                return idFromUrl(response.headers.location);
        });

        newSchema.name = uuid().toLowerCase();
        await axios.put(`http://localhost:${testPort}/v1/schemas/${schemaId}`, newSchema, authOptions)
        .then( async (response) => {
            expect(response.status).toEqual(400);
        });

        await cleanSchema(newSchema.name, schemaId);
    });


    it('should allow POST to existing schema', async () => {
        let newSchema = schemaProvided
        newSchema.name = uuid().toLowerCase();
        const schemaId = await axios.post(`http://localhost:${testPort}/v1/schemas/`, newSchema, authOptions)
            .then( async (response) => {
                expect(response.status).toEqual(201);
                return idFromUrl(response.headers.location);
        });

        await axios.post(`http://localhost:${testPort}/v1/schemas/`, newSchema, authOptions)
        .then( async (response) => {
            expect(response.status).toEqual(400);
        });

        await cleanSchema(newSchema.name, schemaId);
    });

    it('should not allow PUT to non-existant schema', async () => {
        const schemaId = uuid().toLowerCase();

        await axios.put(`http://localhost:${testPort}/v1/schemas/${schemaId}`, schemaProvided, authOptions)
        .then( async (response) => {
            expect(response.status).toEqual(404);
        });       
    });

});