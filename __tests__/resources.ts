test.skip('skip', () => {})

import { SettingsDto } from "../src/models/settingsDto";
import { SchemaDto } from "../src/models/schemaDto";
import * as config from 'config';
import { DynamoDB } from 'aws-sdk';

export const settingsDtoUser: SettingsDto = {
    settings: {

    },
    name: "test",
    id: "12345",
    resource: {
        id: "testUser",
        type: "users"
    }
}

export const settingsDto: SettingsDto = {
    settings: {
        'hello': 'world'
    },
    name: "merchants:internalTestMerchant:test",
    id: "12345",
    resource: {
        id: "internalTestMerchant",
        type: "merchants"
    }
}

export const schemaDto: SchemaDto = {
    schema: {
        schema: {
            "properties": {
            "smaller": { "type": "number"},
            "larger": { "type": "number" },
            "medium2": { "type": "number" }
            }
        },
        name: 'test'
    },
    name: "test",
    id: "12345",
    owner: "admin@cimpress.io",
    lastModifiedTimestamp: "2019-04-30T19:56:42.429Z"
}

export const schemaProvided = {
    schema: {	
        "properties": {
            "smaller": { "type": "number"},
            "larger": { "type": "number" },
            "medium2": { "type": "number" }
        }
    },
    name: "test"
}

export function idFromUrl(url: string): string {
    const locationSplit = url.split('/')
    return locationSplit[locationSplit.length - 1];
}

async function deleteById(id: string, tableName: string): Promise<boolean> {
    const dbClient = new DynamoDB.DocumentClient();

    let params: DynamoDB.DocumentClient.DeleteItemInput = {
        TableName: tableName,
        Key: {
            id
        }
    };

    return await dbClient.delete(params).promise().then(
        () => true
    ).catch(() => false);
};

export async function cleanSchema(nameId: string, schemaId: string): Promise<boolean> {
    const nameRes = await deleteById(nameId, config.get('NamesDb'));

    const schemaRes = await deleteById(schemaId, config.get('SchemaDb'));

    return nameRes && schemaRes;
};

export async function cleanSettings(nameId: string, settingId: string): Promise<boolean> {
    const nameRes = await deleteById(nameId, config.get('NamesDb'));

    const schemaRes = await deleteById(settingId, config.get('SettingDb'));

    return nameRes && schemaRes;
};