import { createServer } from "../src/server";
import * as http from "http";
import * as express from 'express';
import axios, { AxiosRequestConfig } from 'axios';
import { idFromUrl, cleanSettings, settingsDto } from "./resources";
import * as uuid from 'uuid';
import * as aws from 'aws-sdk';
import * as config from 'config'
import { SettingsDto } from "../src/models/settingsDto";
require('dotenv').config();

describe('testing settings endpoints, coam disabled', () => {
    let service: http.Server;
    let server: express.Express;
    const testPort = 3001;
    beforeAll(() => {
        if(process.env.LOCAL) {
            const credentials = new aws.SharedIniFileCredentials({profile: 'documents'});
            aws.config.credentials = credentials;
        }
        aws.config.update({region: config.get('region')});
        server = createServer() as express.Express;
        if (server) {
            service = server.listen(testPort);
        }
    });

    afterAll( () => {
        service.close();
    });

    const noAuthOptions: AxiosRequestConfig = {
        headers: {
            authorization : "no"
         },
         validateStatus: () => true // As long as the request returns, no error will be thrown
    };

    const authOptions: AxiosRequestConfig = {
        headers: {
            authorization : "yes"
         },
         validateStatus: () => true
    };

    it('should not allow post setting unauthorized', async () => {
        const res = await axios.post(`http://localhost:${testPort}/v1/settings/`, settingsDto, noAuthOptions);
        expect(res.status).toEqual(403)
    });

    it('find setting by id', async () => {
        let newSetting = settingsDto
        newSetting.name = uuid().toLowerCase();
        const location = await axios.post(`http://localhost:${testPort}/v1/settings/`, newSetting, authOptions)
            .then(response => {
                expect(response.status).toEqual(201);
                expect(response.headers.location).toBeDefined();
                return response.headers.location;
            });
        const settingId = idFromUrl(location);
        const getResponse = await axios.get(`http://localhost:${testPort}/v1/settings/${settingId}`, authOptions);

        expect(getResponse.status).toEqual(200);
        expect(getResponse.data.id).toEqual(settingId);
        expect(getResponse.data.settings.name).toEqual(newSetting.name);
        expect(getResponse.data.settings).toBeDefined();

        await cleanSettings(newSetting.name, settingId);
    });

    it('find setting by id 404 no auth', async () => {
        const settingId = uuid().toLowerCase();
        const getResponse = await axios.get(`http://localhost:${testPort}/v1/settings/${settingId}`, noAuthOptions);

        expect(getResponse.status).toEqual(404);
    });

    it('find setting by id 404 with auth', async () => {
        const settingId = uuid().toLowerCase();
        const getResponse = await axios.get(`http://localhost:${testPort}/v1/settings/${settingId}`, authOptions);

        expect(getResponse.status).toEqual(404);
    });

    it('find setting by name', async () => {
        let newSetting = settingsDto
        newSetting.name = uuid().toLowerCase();
        await axios.post(`http://localhost:${testPort}/v1/settings/`, newSetting, authOptions)
            .then(response => {
                expect(response.status).toEqual(201);
                expect(response.headers.location).toBeDefined();
            });

        const settingRes = await axios.get(`http://localhost:${testPort}/v1/settings?settingName=${newSetting.name}&resourceType=${newSetting.resource.type}&resourceId=${newSetting.resource.id}`, authOptions);
        expect(settingRes.status).toEqual(200);

        const data = settingRes.data;
        expect(data).toBeDefined();
        expect(data._embedded).toBeDefined();
        expect(data._embedded.item).toBeDefined();

        const items = data._embedded.item as SettingsDto[];
        expect(items.length).toEqual(1);

        const settings = items[0];
        expect(settings.settings.name).toEqual(newSetting.name);
        expect(settings.settings).toBeDefined();
        
        await cleanSettings(newSetting.name, settings.id);
    });

    it('should allow PUT to existing setting', async () => {
        let newSetting = settingsDto
        newSetting.name = uuid().toLowerCase();
        const setttingId = await axios.post(`http://localhost:${testPort}/v1/settings/`, newSetting, authOptions)
            .then( async (response) => {
                expect(response.status).toEqual(201);
                return idFromUrl(response.headers.location);
        });

        await axios.put(`http://localhost:${testPort}/v1/settings/${setttingId}`, newSetting, authOptions)
        .then( async (response) => {
            expect(response.status).toEqual(200);
        });

        await cleanSettings(newSetting.name, setttingId);
    });

    it('should not allow PUT to existing setting to change the setting name', async () => {
        let newSetting = settingsDto
        newSetting.name = uuid().toLowerCase();
        const setttingId = await axios.post(`http://localhost:${testPort}/v1/settings/`, newSetting, authOptions)
            .then( async (response) => {
                expect(response.status).toEqual(201);
                return idFromUrl(response.headers.location);
        });

        newSetting.name = uuid().toLowerCase();

        await axios.put(`http://localhost:${testPort}/v1/settings/${setttingId}`, newSetting, authOptions)
        .then( async (response) => {
            expect(response.status).toEqual(400);
        });

        await cleanSettings(newSetting.name, setttingId);
    });

    it('should not allow POST to existing setting', async () => {
        let newSetting = settingsDto
        newSetting.name = uuid().toLowerCase();
        const setttingId = await axios.post(`http://localhost:${testPort}/v1/settings/`, newSetting, authOptions)
            .then( async (response) => {
                expect(response.status).toEqual(201);
                return idFromUrl(response.headers.location);
        });

        await axios.post(`http://localhost:${testPort}/v1/settings/`, newSetting, authOptions)
        .then( async (response) => {
            expect(response.status).toEqual(400);
        });

        await cleanSettings(newSetting.name, setttingId);
    });

    it('should not allow PUT to non-existant setting', async () => {
        const settingId = uuid().toLowerCase();

        await axios.put(`http://localhost:${testPort}/v1/settings/${settingId}`, settingsDto, authOptions)
        .then( async (response) => {
            expect(response.status).toEqual(404);
        });       
    });

});