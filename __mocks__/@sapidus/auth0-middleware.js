module.exports = {
    getMiddleware: function () {
      return function (req, resp, next) {
        if (req.headers.authorization === 'yes') {
            req.user = {sub: 'authUser', 'https://claims.cimpress.io/canonical_id': "testUser"};
        }
        else {
          req.user = {
            sub: 'badUser',
            'https://claims.cimpress.io/canonical_id': "testUser"
          };
        }
        next()
      }
    }
  }
  