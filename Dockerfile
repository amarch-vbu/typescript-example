FROM node:10-alpine

# Install bash and AWS cli, https://github.com/ughly/alpine-aws-cli
RUN apk --no-cache update && \
    apk --no-cache add bash python py-pip py-setuptools ca-certificates curl groff less && \
    pip --no-cache-dir install awscli && \
    pip --no-cache-dir install pyyaml && \
    rm -rf /var/cache/apk/*

ARG SUMO_URL
ENV SUMO_URL $SUMO_URL

# Create app directory
RUN mkdir -p /usr/app
WORKDIR /usr/app

# Copy app package and run script
ADD ./run.sh /usr/app/run.sh

# Bundle app source, dependencies, and config
COPY ./bundle /usr/app/
COPY ./resources /usr/app/resources
COPY ./node_modules /usr/app/node_modules/
COPY ./config /usr/app/config/

EXPOSE 3000/tcp
RUN ["chmod", "+x", "/usr/app/run.sh"]
ENTRYPOINT [ "/usr/app/run.sh" ]
