#!/bin/bash -e

PROGRAM_ARGS="/usr/app/index.js $PROGRAM_ARGS"

echo Running \`node $PROGRAM_ARGS \`

exec node $PROGRAM_ARGS
