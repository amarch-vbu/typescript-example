# Setting Service

This a service to have resource based settings.

## Getting Started

### Requirements

This system runs on Node 10.

Other requirements:

1. Configuration of CT artifactory
2. Python 2 (optional)

#### Installing Requirements

* To set up the npm repository the command is `npm add-user --registry=https://artifactory.cimpress.io/artifactory/api/npm/npm-logistics-local/ --scope=@sapidus`. You will be promted for a user name, password (api key), and an email.
* Python 2 is an optional dependency for node-gyp. If you do not want python 2 to be your default python executable then run the command `npm config set python python2.7`. This requires python 2.7 to be installed.
* Create a .env file. This file will hold secrets and easily set environment vairables. The two values it should contain are LOCAL=true and a SUMO_URL=https://collectors.sumologic.com
### Running the service

Run the service with the following commands:

```
npm install
npm run compile
npm run dev
```

The service will listen on port 3000 by default

## Deployment

### Infrastructure

* The fargate cluster that this project runs on is defined in: git@cimpress.githost.io:DocDesign/Tools/terraform-common.git.
* To update the service and task definition the /deploy folder contains the terraform files. Once the desired files have been updated then set the default aws credentials to the Documents aws account.
* The CI currently does not run Terraform commands and `terraform plan`/`terraform apply` must be run locally.
